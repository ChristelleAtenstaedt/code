// vhttp is variable. require is a way of saying, import http.
const http = require('http')

const hostname = '127.0.0.1'
const port = 3000

// in that http we have the create server method. request and resolution
const server = http.createServer((req,res) => { //anonymous function
    res.end("This is my NodeJS server")
})

// server will listen to port 3000
server.listen(port, hostname, ()=>{
    console.log(`Server is running at http://${hostname}/${port}/`)
})

// server.listen(4000, ()=>{
//     const {address, port} = server.address() //unpacking
//     console.log(`Server is listening on: http://${address}:${port}`)
// })

