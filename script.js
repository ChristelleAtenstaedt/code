// select the element first and then add interactivity

// document.body.style.backgroundColor = "yellow"
// document.body.style.color = "red"
// document.body.textContent = "Hello, world"

// use only one id per page

//best to use getElementById as it's more general, you can change it to select a class by adding a . in front instead of a #

// const heading = document.querySelector("#heading")
// heading.style.color ="red"
// heading.style.textAlign ="center"
// heading.style.backgroundColor ="yellow"

//DOM => Document Object Model is an API
//Object
//Dictionary => {h1:value}, {key:value} value could be text, styling etc.
//Reads a website and creates a JS object, can access all these html elements.

const txt = document.querySelector("#txt")
const dark = document.querySelector("#dark")
const light = document.querySelector("#light")
const submit = document.querySelector('#submit')
const heading = document.querySelector('#heading')

//aplying event listener to dark variable, applying function
dark.addEventListener('click',
function(){
    document.body.style.backgroundColor = "black"
}
)

light.addEventListener('click',
function(){
    document.body.style.backgroundColor = "white"
}
)
submit.addEventListener('click',
function(){
    heading.textContent = txt.value
})
//when i clieck on this button, perform this method abovr which takes two parameters.