//importing express module
const express = require('express')
//calling express function and saving info in app variable
const app = express()
const port = 3000

//create a route for the root path, the app is app from above and 'get' is a method.
app.get('/', (req,res)=> {
    res.send("Hello, World!!! This is an express server")
})

//
app.listen(port,()=>
{
    console.log(`Server running at http://127.0.0.1:${port}/`)
})